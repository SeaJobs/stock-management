package Project;

public class Loading extends Thread {
	
	String waiting = "Pleas Wait Loading";
	String loading = "........."+"\n";

	@Override
	public void run() {
		System.out.print(waiting);
		for (int i = 0 ; i < loading.length(); i ++) {
			System.out.print(loading.charAt(i));
			try {
				sleep(100);
			} catch ( Exception a){
				System.out.println(a);
			}
		}
	}
}
