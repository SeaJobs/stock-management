package Project;

import java.sql.*;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import java.io.Serializable;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Date;


public class Main implements Serializable{
    static Scanner sc = new Scanner(System.in);
   // static ArrayList<Product> ProductList = new ArrayList<>();
    static int proID ;
    static String proName;
    static double proUnit ;
    static int proStock_Q;
   // static String importedDate;
    
     Product proObj = new Product(proID,proName,proUnit,proStock_Q){};
    public static  String dateImported () {
         DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
         LocalDateTime now = LocalDateTime.now();
         return dtf.format(now);
     }

    public static void groupLogo (){
         
         
    
         int width = 100;
         int height = 18;
         BufferedImage image = new BufferedImage(width, height, 1);
         Graphics g = image.getGraphics();
         g.setFont(new Font("SansSerif", 1, 18));
         Graphics2D graphics = (Graphics2D)g;
         graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
         graphics.drawString("BTB G 4", 14, 15);
    
         for(int y = 0; y < height; ++y) {
             StringBuilder sb = new StringBuilder();
        
             for(int x = 0; x < width; ++x) {
                 sb.append(image.getRGB(x, y) == -16777216 ? " " : "#");
             }
        
             if (!sb.toString().trim().isEmpty()) {
                 System.out.println(sb);
             }
         }
    }
    
    public static void menuOption (){
        
        CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
        Table menuTable = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

        menuTable.setColumnWidth(0,15,50);
        menuTable.setColumnWidth(1,15,50);
        menuTable.setColumnWidth(2,15,50);
        menuTable.setColumnWidth(3,15,50);
        menuTable.setColumnWidth(4,15,50);
        menuTable.setColumnWidth(5,15,50);
        menuTable.setColumnWidth(6,15,50);
        menuTable.setColumnWidth(7,15,50);
        menuTable.setColumnWidth(8,15,50);

        menuTable.addCell("*) Display",center);
        menuTable.addCell("| W) Write",center);
        menuTable.addCell("| R) Read",center);
        menuTable.addCell("| U) Update",center);
        menuTable.addCell("| D) Delete",center);
        menuTable.addCell("| F) First",center);
        menuTable.addCell("| P) Previous",center);
        menuTable.addCell("| N) Next",center);
        menuTable.addCell("| N) Last",center);
    
    
            menuTable.addCell("S) Search",center);
            menuTable.addCell("| G) Goto",center);
            menuTable.addCell("| Se) Set Row",center);
            menuTable.addCell("| Sa) Save",center);
            menuTable.addCell("| Ba) Back up",center);
            menuTable.addCell("| Re) Restore",center);
            menuTable.addCell("| H) Help",center);
            menuTable.addCell("| E) Exit",center);
    
            System.out.println(menuTable.render());
    }
    
    public static void update(ArrayList <Product> arr) throws SQLException, ClassNotFoundException {
        System.out.print("Input Product ID :");
        String pro= sc.nextLine();
        int x = Integer.parseInt(pro);
         try {
             FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
             ObjectInputStream is = new ObjectInputStream(file);
             arr = (ArrayList) is.readObject();
             is.close();
             file.close();
         }
         catch (Exception e){
             System.out.println(e);
         }
         int count =0;
         for(int i= 0; i<arr.size();i++){
             if(x==(arr.get(i).id)){
                 count++;
                 CellStyle center = new CellStyle(CellStyle.HorizontalAlign.left);
                 Table menuTable = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

                 menuTable.setColumnWidth(0,30,50);

                 menuTable.addCell("ID              :   "+String.valueOf(arr.get(i).id),center);
                 menuTable.addCell("Name            :   "+arr.get(i).name,center);
                 menuTable.addCell("UnitPrice       :   "+String.valueOf(arr.get(i).unitPrice),center);
                 menuTable.addCell("Qty             :   "+String.valueOf(arr.get(i).stock_Q),center);
                 menuTable.addCell("ImportedDate    :   "+arr.get(i).importedDate,center);
                 System.out.println(menuTable.render());
                 CellStyle center1 = new CellStyle(CellStyle.HorizontalAlign.center);
                 Table menuTable1 = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

                 menuTable1.setColumnWidth(0,20,100);
                 menuTable1.setColumnWidth(1,20,100);
                 menuTable1.setColumnWidth(2,20,100);
                 menuTable1.setColumnWidth(3,20,100);
                 menuTable1.setColumnWidth(4,20,100);

                 menuTable1.addCell(" 1.Update all ",center1);
                 menuTable1.addCell(" 2.Update name ",center1);
                 menuTable1.addCell(" 3.Update UnitPrice",center1);
                 menuTable1.addCell(" 4.Update stock_Qty",center1);
                 menuTable1.addCell(" 5.Back to menu",center1);
                 System.out.println(menuTable1.render());
                 System.out.print("-->Chose option(1-5):");
                 String str1 = sc.nextLine();
                 int y = Integer.parseInt(str1);
                 switch (y){
                     case 1:
                         System.out.print("-->Update new ID:");
                         String text = sc.nextLine();
                         int id = Integer.parseInt(text);
                         System.out.print("-->Update new name:");
                         String name = sc.nextLine();
                         System.out.print("-->Update new unitPrice:");
                         double pri = sc.nextDouble();
                         System.out.print("-->Update new Qty:");
                         int Qty = sc.nextInt();
                         String ss = sc.nextLine();
                         arr.get(i).id = id;
                         arr.get(i).name =name;
                         arr.get(i).unitPrice=pri;
                         arr.get(i).stock_Q=Qty;
                         save(arr);
                         break;
                     case 2:
                         System.out.print("-->Update new name:");
                         String name1 = sc.nextLine();
                         arr.get(i).name =name1;
                         save(arr);
                         break;
                     case 3:
                         System.out.print("-->Update new unitPrice:");
                         double pri1 = sc.nextDouble();
                         arr.get(i).unitPrice=pri1;
                         save(arr);
                         break;
                     case 4:
                         System.out.print("-->Update new Qty:");
                         String qty1 = sc.nextLine();
                         int Qty1 = Integer.parseInt(qty1);
                         arr.get(i).stock_Q=Qty1;
                         save(arr);
                         break;
                     case 5:
                         return;
                     default:
                         System.out.println("--------Input Invalid----------");
                         return;
                 }


             } else {
                 if(i==arr.size()-1 && count ==0)
                     System.out.println("-->>Can not found this ID!!!!!");
             }
         }

    }
    
    public static void display(ArrayList<Product> arr, int setrow, Connection connection){
         // Display Simple
        //Read arraylist of object from file
//        try {
//            FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt"); //
//            ObjectInputStream is = new ObjectInputStream(file); //
//            arr = (ArrayList) is.readObject();
//            is.close();
//            file.close();
//        }
//        catch (Exception e){
//            System.out.println(e);
//        }
//
//        for(int i=0;i<arr.size();i++){
//           CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
//		   Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
//           t.setColumnWidth(0, 20, 40);
//           t.setColumnWidth(1, 20, 40);
//           t.setColumnWidth(2, 20, 40);
//           t.setColumnWidth(3, 20, 40);
//           t.setColumnWidth(4, 40, 60);
//            if(i==0) {
//                t.addCell("ID", numberStyle);
//                t.addCell("Name", numberStyle);
//                t.addCell("Unit Price", numberStyle);
//                t.addCell("QTY", numberStyle);
//                t.addCell("Imported Date", numberStyle);
//            }
//
//			t.addCell(String.valueOf(arr.get(i).id), numberStyle);
//			t.addCell(arr.get(i).name, numberStyle);
//			t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
//			t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
//			t.addCell(arr.get(i).importedDate, numberStyle);
//			System.out.print(t.render());
//			if(i==arr.size()-1)
//			    System.out.println("\n");
//       }
    
        // Display with Pages
    
//        try {
//            FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");///Users/huang.da.li/Desktop/BackUp/2020-04-03.bak
//            ObjectInputStream is = new ObjectInputStream(file); //E:\JavaApp\StockFile.txt // E:\JavaApp\Stock1.txt
//            arr = (ArrayList) is.readObject();
//            is.close();
//            file.close();
//        }
//        catch (Exception e){
//            System.out.println(e);
//        }
//



        try {
            //3- String stSelect
            String stSelect = "SELECT * FROM product";
            PreparedStatement ps = connection.prepareStatement(stSelect);
            //4-Execute Statement
            ResultSet resultSet = ps.executeQuery();
            System.out.println("All Record From Database ");
            while (resultSet.next()) {
                System.out.println("ID              : " + resultSet.getInt("ID"));
                System.out.println("Name            : " + resultSet.getString("Name"));
                System.out.println("unitPrice       : " + resultSet.getString("unitPrice"));
                System.out.println("Stock_Q         : " + resultSet.getInt("stock_Q"));
                System.out.println("importedDate    : " + resultSet.getString("importedDate"));

            }

//            resultSet.close();
//            ps.close();
//            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        int count =1;
        int x =0;
        for(int i=0;i<arr.size();i++){
            CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
            Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
            t.setColumnWidth(0, 20, 40);
            t.setColumnWidth(1, 20, 40);
            t.setColumnWidth(2, 20, 40);
            t.setColumnWidth(3, 20, 40);
            t.setColumnWidth(4, 40, 60);
            if(i==0 || i==x+setrow) {
                t.addCell("ID", numberStyle);
                t.addCell("Name", numberStyle);
                t.addCell("Unit Price", numberStyle);
                t.addCell("QTY", numberStyle);
                t.addCell("Imported Date", numberStyle);
                x =i;
            }
            t.addCell(String.valueOf(arr.get(i).id), numberStyle);
            t.addCell(arr.get(i).name, numberStyle);
            t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
            t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
            t.addCell(arr.get(i).importedDate, numberStyle);
            System.out.print(t.render());
            if(i==setrow-1 || (i==arr.size()-1 && i<setrow-1)) {
                int y = arr.size()%setrow;
                if(y>0)
                    y=1;
                System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                System.out.println("                                                 page"+count+" of "+(arr.size()/setrow+y));
                System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                count++;
            }else if(i==x+setrow-1){
                int y = arr.size()%setrow;
                if(y>0)
                    y=1;
                System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                System.out.println("                                                 page"+count+" of "+(arr.size()/setrow+y));
                System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                count++;
            }
            else if( i==arr.size()-1){
                int y = arr.size()%setrow;
                if(y>0)
                    y=1;
                System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                System.out.println("                                                 page"+count+" of "+(arr.size()/setrow+y));
                System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
            }
        
        }
    }
    
    public static void First (ArrayList<Product> arr, int setrow){
         try {
             FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
             ObjectInputStream is = new ObjectInputStream(file);
             arr = (ArrayList) is.readObject();
             is.close();
             file.close();
         }
         catch (Exception e){
             System.out.println(e);
         }
         int count =1;
         if(arr.size()>setrow) {
             for (int i = 0; i < setrow; i++) {
                 CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                 Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
                 t.setColumnWidth(0, 20, 40);
                 t.setColumnWidth(1, 20, 40);
                 t.setColumnWidth(2, 20, 40);
                 t.setColumnWidth(3, 20, 40);
                 t.setColumnWidth(4, 40, 60);
                 if (i == 0) {
                     t.addCell("ID", numberStyle);
                     t.addCell("Name", numberStyle);
                     t.addCell("Unit Price", numberStyle);
                     t.addCell("QTY", numberStyle);
                     t.addCell("Imported Date", numberStyle);
                 }
                 t.addCell(String.valueOf(arr.get(i).id), numberStyle);
                 t.addCell(arr.get(i).name, numberStyle);
                 t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
                 t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
                 t.addCell(arr.get(i).importedDate, numberStyle);
                 System.out.print(t.render());
                 if (i == setrow-1 || (i == arr.size() - 1 && i < setrow-1)) {
                     int y = arr.size() % setrow;
                     if (y > 0)
                         y = 1;
                     System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                     System.out.println("                                                 page" + count + " of " + (arr.size() / 3 + y));
                     System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                 }
             }
         }else {
             for (int i = 0; i < arr.size(); i++) {
                 CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                 Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
                 t.setColumnWidth(0, 20, 40);
                 t.setColumnWidth(1, 20, 40);
                 t.setColumnWidth(2, 20, 40);
                 t.setColumnWidth(3, 20, 40);
                 t.setColumnWidth(4, 40, 60);
                 if (i == 0) {
                     t.addCell("ID", numberStyle);
                     t.addCell("Name", numberStyle);
                     t.addCell("Unit Price", numberStyle);
                     t.addCell("QTY", numberStyle);
                     t.addCell("Imported Date", numberStyle);
                 }
                 t.addCell(String.valueOf(arr.get(i).id), numberStyle);
                 t.addCell(arr.get(i).name, numberStyle);
                 t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
                 t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
                 t.addCell(arr.get(i).importedDate, numberStyle);
                 System.out.print(t.render());
                 if (i == setrow-1 || (i == arr.size() - 1 && i < setrow-1)) {
                     int y = arr.size() % setrow;
                     if (y > 0)
                         y = 1;
                     System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                     System.out.println("                                                 page" + count + " of " + (arr.size() / setrow + y));
                     System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                 }
             }
         }
     }
    // Boran Jork, [2 May 2021 at 16:06:31]:
    public static void Last(ArrayList<Product> arr, int setrow) {
         try {
             FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
             ObjectInputStream is = new ObjectInputStream(file);
             arr = (ArrayList) is.readObject();
             is.close();
             file.close();
         } catch (Exception e) {
             System.out.println(e);
         }
         int x=0;
         int count =1;
         int si = arr.size()%setrow;
         CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
         Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
         t.setColumnWidth(0, 20, 40);
         t.setColumnWidth(1, 20, 40);
         t.setColumnWidth(2, 20, 40);
         t.setColumnWidth(3, 20, 40);
         t.setColumnWidth(4, 40, 60);
         if(arr.size()>setrow && si==0) {
             for (int i =(arr.size()-setrow);i <arr.size(); i++) {
                 if (i ==(arr.size()-setrow)) {
                     t.addCell("ID", numberStyle);
                     t.addCell("Name", numberStyle);
                     t.addCell("Unit Price", numberStyle);
                     t.addCell("QTY", numberStyle);
                     t.addCell("Imported Date", numberStyle);
                     x=i;
                 }
                 t.addCell(String.valueOf(arr.get(i).id), numberStyle);
                 t.addCell(arr.get(i).name, numberStyle);
                 t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
                 t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
                 t.addCell(arr.get(i).importedDate, numberStyle);
                 if(i==arr.size()-1) {
                     System.out.println(t.render());
                     System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                     System.out.println("                                                 page" + arr.size() / setrow + " of " + (arr.size() / setrow));
                     System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                 }
             }
         }else if(arr.size()<=setrow) {
             for (int i = 0; i < arr.size(); i++) {
                 if (i == 0) {
                     t.addCell("ID", numberStyle);
                     t.addCell("Name", numberStyle);
                     t.addCell("Unit Price", numberStyle);
                     t.addCell("QTY", numberStyle);
                     t.addCell("Imported Date", numberStyle);
                 }
                 t.addCell(String.valueOf(arr.get(i).id), numberStyle);
                 t.addCell(arr.get(i).name, numberStyle);
                 t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
                 t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
                 t.addCell(arr.get(i).importedDate, numberStyle);
                 if (i == (arr.size()-1)) {
                     System.out.print(t.render());
                     int  y = 1;
                     System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                     System.out.println("                                                 page" + y+ " of " + y);
                     System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                 }
             }
         }else if(arr.size()>setrow && si!=0){
             for (int i =(arr.size()-si);i <arr.size(); i++) {
                 if (i ==(arr.size()-si)) {
                     t.addCell("ID", numberStyle);
                     t.addCell("Name", numberStyle);
                     t.addCell("Unit Price", numberStyle);
                     t.addCell("QTY", numberStyle);
                     t.addCell("Imported Date", numberStyle);
                     x=i;
                 }
                 t.addCell(String.valueOf(arr.get(i).id), numberStyle);
                 t.addCell(arr.get(i).name, numberStyle);
                 t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
                 t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
                 t.addCell(arr.get(i).importedDate, numberStyle);
                 int y = 1;
                 if(i== arr.size()-1)
                     System.out.println(t.render());
                 System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                 System.out.println("                                                 page"+(arr.size()/setrow+y)+" of "+(arr.size()/setrow+y));
                 System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
             }
            
         }
     }
    
    public static void save(ArrayList<Product> arr) throws ClassNotFoundException, SQLException {
       // Write arraylist to file
//        try {
//            FileOutputStream file = new FileOutputStream("E:\\JavaApp\\StockFile.txt");//E:\JavaApp\StockFile.txt
//            ObjectOutputStream os = new ObjectOutputStream(file);
//            os.writeObject(arr);
//            os.close();
//            file.close();
//        }
//        catch (IOException ioe) {
//            ioe.printStackTrace();
//        }


        // Save to Database

        Class.forName("org.postgresql.Driver");
        Connection  connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/JavaMiniProject2");//"jdbc:postgresql://localhost:5432/JavaMiniProject2"
        String insert = "INSERT INTO product2(id,name,price,qty,imported_date) VALUES(?,?,?,?,?)";  //"jdbc:postgresql://localhost:5432/postgres","postgres","143200"
        PreparedStatement ps1 = connection.prepareStatement(insert);
        for(int i=0;i<arr.size();i++){
            ps1.setInt(1,arr.get(i).id);
            ps1.setString(2,arr.get(i).name);
            ps1.setDouble(3,arr.get(i).unitPrice);
            ps1.setInt(4,arr.get(i).stock_Q);
            ps1.setString(5,arr.get(i).importedDate);
            ps1.executeUpdate();

        }


    }
    
    public static void write(ArrayList<Product> arr, Connection connection) throws SQLException, ClassNotFoundException {
//        try {
//            FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt"); // /Users/huang.da.li/Desktop/BackUp/2020-04-03.bak
//            ObjectInputStream is = new ObjectInputStream(file); //E:\JavaApp\StockFile.txt
//            arr = (ArrayList) is.readObject();      //E:\JavaApp\Stock1.txt
//            is.close();
//            file.close();
//        }
//        catch (Exception e){
//            System.out.println(e);
//        }
        String stSelect = "SELECT COUNT(id) FROM product";
        Statement ps1 = connection.createStatement();
        ResultSet resultSet = ps1.executeQuery(stSelect);
        resultSet.next();
        int totalRecord = resultSet.getInt(1);

        //int x = arr.get(arr.size()-1).id +1;
        int x = totalRecord +1;
        System.out.println("-->Product ID :"+x);
         System.out.print("-->input Product's name:");
         String name =sc.nextLine();
         System.out.print("-->Input unitPrice:");
         double pr = sc.nextDouble();
         System.out.print("-->input Qty :");
         int qty = sc.nextInt();
         //String str =sc.nextLine();
        // String date = dateImported().toString();

        try{
            String stInsert = "INSERT INTO product (id, name, unitPrice, stock_Q, importedDate) VALUES (?,?,?,?,?)";

            PreparedStatement ps = connection.prepareStatement(stInsert);
            ps.setInt(1, x);
            ps.setString(2, name);
            ps.setDouble(3, pr);
            ps.setInt(4,qty);
            long millis=System.currentTimeMillis();
            java.sql.Date date=new java.sql.Date(millis);
            ps.setDate(5,date);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        CellStyle center = new CellStyle(CellStyle.HorizontalAlign.left);
        Table menuTable = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
        menuTable.setColumnWidth(0,30,50);
//
//        menuTable.addCell("ID               : ",left);
//        menuTable.addCell(String.valueOf(proObj.getId()),left);
//        menuTable.addCell("Name             :",left);
//        menuTable.addCell(proObj.name,left);
//        menuTable.addCell("Unit Price       :",left);
//        menuTable.addCell(String.valueOf(proObj.unitPrice),left);
//        menuTable.addCell("QTY              :",left);
//        menuTable.addCell(String.valueOf(proObj.stock_Q),left);
//        menuTable.addCell("Imported Date    :",left);
//        menuTable.addCell(proObj.importedDate,left);

        menuTable.addCell("ID               :   "+ String.valueOf(x),center);
        menuTable.addCell("Name             :   "+name,center);
        menuTable.addCell("UnitPrice        :   "+String.valueOf(pr),center);
        menuTable.addCell("Qty              :   "+String.valueOf(qty),center);
        menuTable.addCell("Imported Date    :   "+dateImported(),center);
        System.out.println(menuTable.render());
        System.out.print("Do you want to save this record?\t");
        System.out.println("Press N[no] to cancel / Press Y[yes] to Save");
        System.out.print("-->chose option:");
        String op = sc.next();
        String  yes ="y";
        String no = "n";
        if(op.compareToIgnoreCase(yes)==0){
            arr.add(new Product(x,name,pr,qty));
           save(arr);

                System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                System.out.println(x+ " was added to Database successfully ");
                System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");

        } else return;
    }
    
    public static void search(ArrayList<Product> arr){
         System.out.print("-->Input product's name:");
         String str = sc.nextLine();
         try {
             FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
             ObjectInputStream is = new ObjectInputStream(file);
             arr = (ArrayList) is.readObject();
             is.close();
             file.close();
         }
         catch (Exception e){
             System.out.println(e);
         }
         int count =0;
         for(int i= 0; i<arr.size();i++){
             if(str.compareToIgnoreCase(arr.get(i).name)==0){
                 count++;
                 CellStyle center = new CellStyle(CellStyle.HorizontalAlign.left);
                 Table menuTable = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

                 menuTable.setColumnWidth(0,30,50);

                 menuTable.addCell("ID              :   "+String.valueOf(arr.get(i).id),center);
                 menuTable.addCell("Name            :   "+arr.get(i).name,center);
                 menuTable.addCell("UnitPrice       :   "+String.valueOf(arr.get(i).unitPrice),center);
                 menuTable.addCell("Qty             :   "+String.valueOf(arr.get(i).stock_Q),center);
                 menuTable.addCell("ImportedDate    :   "+arr.get(i).importedDate,center);
                 System.out.println(menuTable.render());
             }
             else {
                 if(i==arr.size()-1 && count==0)
                     System.out.println("-->>Can not found this name!!!!!");
             }
         }

     }

    public static void read(ArrayList<Product> arr){
         System.out.print("-->Input product's id:");
         String str = sc.nextLine();
         int x = Integer.parseInt(str);
         try {
             FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
             ObjectInputStream is = new ObjectInputStream(file);
             arr = (ArrayList) is.readObject();
             is.close();
             file.close();
         }
         catch (Exception e){
             System.out.println(e);
         }
         int count =0;
         for(int i= 0; i<arr.size();i++){
             if(x==(arr.get(i).id)){
                 count++;
                 CellStyle center = new CellStyle(CellStyle.HorizontalAlign.left);
                 Table menuTable = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
                 
            
                 menuTable.setColumnWidth(0,30,50);

                 menuTable.addCell("ID              :   "+String.valueOf(arr.get(i).id),center);
                 menuTable.addCell("Name            :   "+arr.get(i).name,center);
                 menuTable.addCell("UnitPrice       :   "+String.valueOf(arr.get(i).unitPrice),center);
                 menuTable.addCell("Qty             :   "+String.valueOf(arr.get(i).stock_Q),center);
                 menuTable.addCell("ImportedDate    :   "+arr.get(i).importedDate,center);
                 System.out.println(menuTable.render());
             }
             else {
                 if(i==arr.size()-1 && count==0)
                     
                     System.out.println("-->>Can not found this ID!!!!!");
             }
         }

     }
     
    public static void delete(ArrayList<Product> arr) throws SQLException, ClassNotFoundException {
         System.out.print("-->Input Product's ID to delete:");
         String str = sc.nextLine();
         int x= Integer.parseInt(str);
        try {
            FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
            ObjectInputStream is = new ObjectInputStream(file);
            arr = (ArrayList) is.readObject();
            is.close();
            file.close();
        }
        catch (Exception e){
            System.out.println(e);
        }
        int count =0;
        for(int i= 0; i<arr.size();i++){
            if(x==(arr.get(i).id)){
                count++;
                CellStyle center = new CellStyle(CellStyle.HorizontalAlign.left);
                Table menuTable = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

                menuTable.setColumnWidth(0,30,50);

                menuTable.addCell("ID               :   "+String.valueOf(arr.get(i).id),center);
                menuTable.addCell("Name             :   "+arr.get(i).name,center);
                menuTable.addCell("UnitPrice        :   "+String.valueOf(arr.get(i).unitPrice),center);
                menuTable.addCell("Qty              :   "+String.valueOf(arr.get(i).stock_Q),center);
                menuTable.addCell("ImportedDate     :   "+arr.get(i).importedDate,center);
                System.out.println(menuTable.render());
                
                System.out.print("Do you want to delete it?\t");
                System.out.println("Press N[no] to cancel / Press Y[yes] to delete");
                System.out.print("-->chose option:");
                String op = sc.nextLine();
                String  yes ="y";
                String no = "n";
                if(op.compareToIgnoreCase(yes)==0){
                    arr.remove(i);
                    save(arr);
                    System.out.println("Successful to deleted !");
                }
                else return;
            }
            else {
                if(count==0 && i==arr.size()-1)
                    System.out.println("-->>Can not found this ID!!!");
            }
        }

    }
    
    public static void Goto(ArrayList<Product> arr , int setrow){
        
         try {
             FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
             ObjectInputStream is = new ObjectInputStream(file);
             arr = (ArrayList) is.readObject();
             is.close();
             file.close();
         }
         catch (Exception e){
             System.out.println(e);
         }
         System.out.print("-->Input Page number :");
         int x =sc.nextInt();
         String sr = sc.nextLine();
         int ss = arr.size()%setrow;
         int firstIndex = (x-1)*setrow;
         int lastIndex = firstIndex +setrow;
         if( x<=1)
             First(arr,setrow);
         else if( x==arr.size()/setrow && ss==0 )
             Last(arr,setrow);
         else if(ss!=0 && x>arr.size()/setrow)
             Last(arr,setrow);
         else if(x>1 && arr.size()>setrow){
             for(int i=firstIndex;i<lastIndex;i++){
                 CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
                 Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
                 t.setColumnWidth(0, 20, 40);
                 t.setColumnWidth(1, 20, 40);
                 t.setColumnWidth(2, 20, 40);
                 t.setColumnWidth(3, 20, 40);
                 t.setColumnWidth(4, 40, 60);
                 if (i == firstIndex) {
                     t.addCell("ID", numberStyle);
                     t.addCell("Name", numberStyle);
                     t.addCell("Unit Price", numberStyle);
                     t.addCell("QTY", numberStyle);
                     t.addCell("Imported Date", numberStyle);
                 }
                 t.addCell(String.valueOf(arr.get(i).id), numberStyle);
                 t.addCell(arr.get(i).name, numberStyle);
                 t.addCell(String.valueOf(arr.get(i).unitPrice), numberStyle);
                 t.addCell(String.valueOf(arr.get(i).stock_Q), numberStyle);
                 t.addCell(arr.get(i).importedDate, numberStyle);
                 System.out.print(t.render());
                 if(i==lastIndex-1) {
                     int plus = arr.size()%setrow;
                     if(plus>0){
                         plus=1;
                     }
                     System.out.println("\n@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                     System.out.println("                                                 page" + x + " of " + (arr.size() / setrow + plus));
                     System.out.println("@~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~@");
                 }
             }
         }
        
     }
     
    public static void backUp(ArrayList<Product>arr) {
        //Copy from Source to Target
        String sourcePath = "E:\\JavaApp\\StockFile.txt";
        String targetPath = "/Users/huang.da.li/Desktop/BackUp/"+dateImported()+".bak";
        Path source = Paths.get(sourcePath);
        Path target = Paths.get(targetPath);
    
        System.out.print("Do you Really want to Backup ?\t");
        System.out.println("Press N[no] to cancel / Press Y[yes] to back up");
        System.out.print("=> chose option : ");
        String op = sc.nextLine();
        String  yes ="y";
        String no = "n";
        if(op.compareToIgnoreCase(yes)==0){
            try {
                Files.copy(source, target);
                System.out.println("Successfully Back up to "+ targetPath);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        
         // Write Direct to File
//        String bakPath = "/Users/huang.da.li/Desktop/BackUp/"+dateImported()+".bak";
//        try {
//            FileOutputStream file = new FileOutputStream(bakPath);
//            ObjectOutputStream os = new ObjectOutputStream(file);
//            os.writeObject(arr);
//            os.close();
//            file.close();
//            System.out.println("Successfully Back up to "+ bakPath);
//        }
//        catch (IOException ioe) {
//            ioe.printStackTrace();
//        }
    }
    
//     public static void M10(ArrayList<Product> arr){
//         try {
//             FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt"); //E:\JavaApp\StockFile.txt
//             ObjectInputStream is = new ObjectInputStream(file);
//             arr = (ArrayList) is.readObject();
//             is.close();
//             file.close();
//             arr.clear();
//
////             long startTime = System.currentTimeMillis();
////             Thread[] threads = new Thread[5];
////             for (int i = 0; i < 5; i++) {
////                 threads[i].start();
////             }
////             for (Thread thread : threads) {
////                 thread.join();
////             }
////             long stopTime = System.currentTimeMillis();
////             System.out.println(stopTime - startTime);
////
//
//             for(int i=0;i<2000;i++){
//                 arr.add(new Product(i+1,"new 10M",10,100));
//                 save(arr);
//             }
//         }
//         catch (Exception e){
//             System.out.println("Error Here");
//             System.out.println(e);
//         }
//
//     }
     static String folderPath = "/Users/huang.da.li/Desktop/BackUp";
    public static void restore (ArrayList<Product>arr, int setrow) throws FileNotFoundException,NullPointerException{
        File folder = new File(folderPath);
        File[] listOfFiles = folder.listFiles();
       // System.out.println(Arrays.toString(folder.listFiles()));
        
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File BackedUP " + listOfFiles[i].getName());
                //System.out.println(listOfFiles[0]); index of file
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
    
        System.out.print("****** Please choose from 1 to "+(listOfFiles.length-1)+" file to Restore : ");
        int chooseFile = sc.nextInt();
        try {
            //Copy from Source to Target
            String targetPath = "E:\\JavaApp\\StockFile.txt";
            String  sourcePath = ""+listOfFiles[chooseFile];
            System.out.println(sourcePath);
            Path source = Paths.get(sourcePath);
            Path target = Paths.get(targetPath);
            FileInputStream file = new FileInputStream(sourcePath);
            ObjectInputStream is = new ObjectInputStream(file);
            arr = (ArrayList) is.readObject();
    
    
            System.out.print("Do you Really want to Restore it ?\t");
            System.out.println("Press N[no] to cancel / Press Y[yes] to Restore");
            System.out.print("=> chose option : ");
            String op = sc.next();
            String  yes ="y";
            String no = "n";
            if(op.compareToIgnoreCase(yes)==0){
                try {
                    //Fixed File Existing => .bak -> .txt
                    Files.copy(source, target, StandardCopyOption.COPY_ATTRIBUTES,StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("Successfully Restore ! "+ sourcePath);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            
            //display(arr, setrow);  Close for Test
            is.close();
            file.close();
            System.out.println("Successfully Restored !");
           
        }
        catch (Exception e){
            System.out.println(e);
        }
    }
    
    public static void help() {
         Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
         t.setColumnWidth(0, 20, 100);
         t.addCell("                 HELP : ");
         t.addCell("1.      Press    *: Display all record of Products");
         t.addCell("2.      Press    w/W: Add New Products");
         t.addCell("3.      Press    r/R: Display content of Products");
         t.addCell("4.      Press    u/U: Update Products");
         t.addCell("5.      Press    d/D: Delete Products");
         t.addCell("6.      Press    s/S: Search Products by Name");
         t.addCell("7.      Press    g/G: Goto Specific Page");
         t.addCell("8.      Press    e/E: Exit System");
         t.addCell("9.      Press    h/H: Help");
         t.addCell("10.     Press    Sa/SA/Sa: Save");
         t.addCell("11.     Press    Se/SE: Set Row");
         t.addCell("12.     Press    Ba/BA/bA: Back up ");
         t.addCell("13.     Press    Re/RE/rE: Restore");
         t.addCell("11.     Press    w#proName-unitPrice-qty    : shortcut for add new product");
         t.addCell("12.     Press    d#proID    : shortcut for delete product by ID");
         t.addCell("13.     Press    r#proID    : shortcut for read product by ID");
         System.out.println(t.render());
     }
     
    public static void exitProgram (){
        System.out.println("Good Bye !!");
        System.exit(0);
        
    }
    
        public static void main (String[]args) throws IOException, InterruptedException, SQLException, ClassNotFoundException {

            ConnectionDB connection = new ConnectionDB();
            Connection connection1 = connection.getConnectionDB();

            Scanner sc = new Scanner(System.in);
             Loading loadObj = new Loading();
            ArrayList<Product> arr = new ArrayList();
//            //10M Record
//            TenMillionRecord T = new TenMillionRecord();
//            long startTime = System.currentTimeMillis();
//            Thread[] threads = new Thread[5];
//            for (int i = 0; i < 5; i++) {
//                threads[i] = new Thread(T);
//                threads[i].start();
//                //T.run();
//            }
//            long stopTime = System.currentTimeMillis();
//           // System.out.println(stopTime - startTime);
//
            System.out.println("\n");
            System.out.println("                                Welcome to ");
            System.out.println("                            Stock Management ");
            System.out.println();
            groupLogo();
            loadObj.start();
            try {
                loadObj.join();
            } catch (Exception e) {
                System.out.println(e);
            }
            long startTime = System.currentTimeMillis();
            try {
                FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");///Users/huang.da.li/Desktop/BackUp/2020-04-03.bak
                ObjectInputStream is = new ObjectInputStream(file); //E:\JavaApp\StockFile.txt // E:\JavaApp\Stock1.txt
                arr = (ArrayList<Product>) is.readObject();
                is.close();
                file.close();
            }
            catch (Exception e){
                System.out.println(e);
            }
            long stopTime = System.currentTimeMillis();
            
             System.out.println("Current Time Loading   : "+(stopTime - startTime)+"ms");
            
             //Initialize and add object of arraylist
             
             Product obj = new Product(1, "cocacola", 12.4, 10);
             Product obj1 = new Product(2, "Pepsi", 12.5, 15);
             Product obj2 = new Product(3, "Sting", 10, 16);
             arr.add(obj);
             arr.add(obj1);
             arr.add(obj2);


             // Add object to Database


            int setrow = 3;
             while (true) {
                 menuOption();
                 System.out.print("Command => ");
                 String str = sc.nextLine();
                 String dis = "*", wr = "w", r = "r", s = "s", de = "d", up = "u", h = "h",_10M = "_10M";
                 String ba = "ba", re = "re", f = "f",l= "l",e = "e",g = "g",sa = "sa",se = "se";
        
                 if (str.compareToIgnoreCase(dis) == 0)
                     display(arr,setrow,connection1);
                 else if (str.compareToIgnoreCase(wr) == 0)
                     write(arr,connection1);
                 else if (str.compareToIgnoreCase(r) == 0)
                     read(arr);
                 else if (str.compareToIgnoreCase(s) == 0)
                     search(arr);
                 else if (str.compareToIgnoreCase(de) == 0)
                     delete(arr);
                 else if (str.compareToIgnoreCase(up) == 0)
                     update(arr);
                 else if (str.compareToIgnoreCase(h) == 0)
                     help();
                 else if (str.compareToIgnoreCase(ba) == 0)
                     backUp(arr);
                 else if (str.compareToIgnoreCase(re) == 0)
                     restore(arr,setrow);
                 else if (str.compareToIgnoreCase(f) == 0)
                     First(arr,setrow);
                 else if (str.compareToIgnoreCase(l) == 0)
                     Last(arr,setrow);
                 else if (str.compareToIgnoreCase(g) == 0)
                     Goto(arr,setrow);
                 else if (str.compareToIgnoreCase(e) == 0)
                     exitProgram();
//                 else if (str.compareToIgnoreCase(_10M) == 0)
//                     M10(arr);
                 else if (str.compareToIgnoreCase(se) == 0){
                     System.out.print("-->>> Input row set :");
                     String scr =sc.nextLine();
                     setrow = Integer.parseInt(scr);
                 }
                 else if (str.compareToIgnoreCase(sa) == 0)
                     System.out.println("We Combined ( Write & Save) inside INSTEAD !");
                     
             }
    }
 }
 
 
 


