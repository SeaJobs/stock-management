package Project;
import java.sql.*;

    class ConnectionDB {

        public ConnectionDB() {
        }

        public Connection getConnectionDB () throws SQLException {

            Connection connection = null;
            try {
                // 1- Load Driver
                Class.forName("org.postgresql.Driver");

                //2- connect to database
                connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/JavaMiniProject2",
                        "postgres", "1234");

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            return connection;
        }
    }
