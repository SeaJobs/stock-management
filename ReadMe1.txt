				Welcome to Stock Management

       KSHRD 9th Generation => 03 May 2021 23:10PM

       Class BTB
       GROUP 4
       Members :
       1) EM THARY
       2) Boran Jork
       3) RON VIREAK

       GitLab : https://gitlab.com/SeaJobs/stock-management

       About Program

       ║ 1.      Press    *: Display all record of Products                         ║
       ║ 2.      Press    w: Add New Products                                       ║
       ║ 3.      Press    r: Display content of Products                            ║
       ║ 4.      Press    u: Update Products                                        ║
       ║ 5.      Press    d: Delete Products                                        ║
       ║ 6.      Press    s: Search Products by Name                                ║
       ║ 7.      Press    g: Goto Specific Page                                     ║
       ║ 8.      Press    Sa: Save                                                  ║
       ║ 9.      Press    Se: Set Row                                               ║
       ║ 10.     Press    Ba: Back up                                               ║
       ║ 10.     Press    Re: Restore                                               ║
       ║ 10.     Press    E: Exit System                                            ║
       ║ 10.     Press    H: Help                                                   ║
       ║ 11.     Press    w#proName-unitPrice-qty    : shortcut for add new product ║
       ║ 12.     Press    d#proID    : shortcut for delete product by ID            ║
       ║ 12.     Press    r#proID    : shortcut for read product by ID              ║